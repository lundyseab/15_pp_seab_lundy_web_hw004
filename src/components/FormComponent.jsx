import React, { Component } from 'react'
import TableComponent from './TableComponent';

export default class FormComponent extends Component {
  constructor(){

    super();
    this.state = {
      person: [
        {id : 1, email: "seablundy@gmail.com", username: "lundy", age: 21, status : "Pending"},
      ],
      newEmail: "",
      newUsername: "",
      newAge: "",
      registerOnClick : false,
    };
  };


  handleEmailInput = (input) => {
    this.setState({
      newEmail: input.target.value,
    });
  };
  
  handleUserNameInput = (input) => {
    this.setState({
      newUsername: input.target.value,
    });
  };

  handleAgeInput = (input) => {
    this.setState({
      newAge: input.target.value,
    });
  };

  handleRegister = () => {
    let email = this.state.newEmail;
    let username = this.state.newUsername;
    let age = this.state.newAge;

    const newPerson ={
      id: this.state.person.length + 1,
      email: email,
      username: username,
      age: age,
      status: "Pending"

    }
    this.setState(
      {
        person: [...this.state.person, newPerson],
        registerOnClick : true,
      },
    );
  };

  handleStatus = (e)=>{
    var st = this.state.person.map((item)=>{
      if(e === item.id){
        return {...item, status:item.status === "Done"? "Pending":"Done"}
      }
      return item;
    });
    this.setState({person:st});
  }

  render() {
    return (
      <div>
        <form>
          {/* Title */}
          <div className="mb-4">
            <h2 className=" text-5xl font-bold mt-2 mb-2 ">
              <span className="text-transparent bg-clip-text bg-gradient-to-r from-blue-500 to-pink-500">Please Fill Your </span>Information
            </h2>
          </div>

          {/* Your Email */}
          <div className="mb-4"> 
            <label className="block text-left text-gray-700 font-bold mb-2" htmlFor="email">
              Your Email
            </label>
            <label class="relative block">
              <span class="sr-only">Search</span>
              <span class="absolute inset-y-0 left-0 flex items-center pl-2 w-10 p-2">
                <i class="fa fa-envelope icon text-gray-600 m-1"></i>
                  
              </span>
              <input class="placeholder:text-gray-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 
              shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
               placeholder="lundy@gmail.com" id="email" type="email" name="email" onChange={this.handleEmailInput}/>
            </label>
          </div>
            
          {/* Username */}
          <div className="mb-4">
            <label className="block text-left text-gray-700 font-bold mb-2" htmlFor="username">
              Username
            </label>

            <div class="flex">
              <div className="bg-slate-200 w-10 p-2 h-auto rounded-l-md shadow-sm">
                <i class="fas fa-at text-gray-600 "></i>
                </div>
                <input class="placeholder:text-gray-400 block bg-white w-full border border-slate-300 rounded-l-none
                 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm" 
                placeholder="Lundy" type="text" id="username" name="username" onChange={this.handleUserNameInput}/>
              </div>
            </div>

          {/* Age */}
          <div className="mb-4">
            <label className="block text-left text-gray-700 font-bold mb-2" htmlFor="age">
              Age
            </label>

            <div class="flex">
              <div className="bg-slate-200 w-10 p-2 h-auto rounded-l-md shadow-sm">
                <img src="https://img.icons8.com/emoji/48/null/heart-suit.png" alt='heart'/>
              </div>
              <input class="placeholder:text-gray-400 block bg-white w-full border border-slate-300 rounded-l-none rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm" 
              placeholder="21" id="age" type="text" name="age" onChange={this.handleAgeInput}/>
            </div>
          </div>
          
          {/* Register button */}
          <div>
            <button className={` w-60 bg-gradient-to-b ${this.state.registerOnClick?'from-white to-white text-black border-sky-600 border-2 hover:text-white hover:from-sky-300 hover:to-blue-500 hover:border-sky-300':
            'from-purple-600 to-blue-500 text-white'}  font-bold py-2 px-4 rounded hover:shadow-lg hover:outline-none hover:border-sky-600 hover:ring-sky-600 hover:ring-1 sm:text-sm`}
            onClick={this.handleRegister} id="btn-register" type="button">
              Register
            </button>
          </div>
        </form>

        {/* Table */}
        <div className="mt-8">
            <TableComponent info = {this.state.person} statusChange={this.handleStatus}/>
        </div>
      </div>
    )
  }
}
