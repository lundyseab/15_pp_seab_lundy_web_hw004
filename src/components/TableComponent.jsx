import React, { Component } from 'react'
import Swal from "sweetalert2";
import 'animate.css';

export default class TableComponent extends Component {

  //Sweet Alert Function
  showMore = (id, email, username, age, status) =>{
    Swal.fire({
      title: `ID: ${id}\nEmail: ${email} \nUsername: ${username} \nAge: ${age} \nStatus: ${status}`,
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    })
  };

  render() {
    return (
      <div>
        <table className="table-auto w-full m-auto">
          <thead>
            {/* First Row */}
            <tr>
              <th className="px-4 py-2 md:p-px">ID</th>
              <th className="px-4 py-2">EMAIL</th>
              <th className="px-4 py-2">USERNAME</th>
              <th className="px-4 py-2">AGE</th>
              <th className="px-4 py-2">ACTION</th>
            </tr>
          </thead>
          <tbody className="rounded-lg">
            {this.props.info.map((data) =>(
              <tr key={data.id} className={`${(data.id%2 === 0)?' bg-cyan-400 text-white ':'bg-gray-100 text-black'} font-medium h-16 md:p-px md:h-auto`}>
              <td className="border px-4 py-2">{data.id}</td>
              <td className="border px-4 py-2">{(data.email !=="")?data.email:"null"}</td>
              <td className="border px-4 py-2">{(data.username !=="")?data.username:"null"}</td>
              <td className="border px-4 py-2">{(data.age !=="")?data.age:"null"}</td>
              <td className="border border-r-0">
                <div>
                  <button className={`w-56 bg-gradient-to-b ${(data.status === "Pending")?' from-red-500 to-pink-600':' from-emerald-500 to-green-600'}  text-white
                   font-bold py-2 rounded m-4 hover:shadow-lg hover:outline-none hover:border-sky-600 hover:ring-sky-400 hover:ring-1 sm:text-sm`}
                   onClick={()=>{this.props.statusChange(data.id)}}>
                    {data.status}
                  </button>
                  <button className=" w-56 bg-gradient-to-b from-sky-400 to-blue-600 text-white font-bold py-2 rounded
                  hover:shadow-lg hover:outline-none hover:border-sky-600 hover:ring-sky-600 hover:ring-1 sm:text-sm" onClick={()=>{this.showMore(
                    data.id, data.email, data.username, data.age, data.status
                    )}}>
                    Show More
                  </button>
                </div>
              </td>
            </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}
