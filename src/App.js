import './App.css';
import FormComponent from './components/FormComponent';
function App() {
  return (
    <div className="App">
      <div className="w-full m-auto">
        <div className="p-12" >
          <FormComponent/>
        </div>
      </div>
    </div>
  );
}

export default App;

